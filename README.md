Projeto que simula um "e-commerce" de funcionários de empresas mundiais. Cada funcionário possui um preço e pode ser comprado. Nesse caso específico utilizei os funcionários do Facebook.

## Caso queira ver o projeto live

Acesse https://front-end-challenge-otavio.netlify.com/

## Alguns disclaimers, features e highlights do projeto
- Não sou indeciso, usei por opção várias "coisas" misturadas no projeto: Class e Function components, CSS e styled-components ...

- Responsividade: tá ok

- Conexão com a API do Github para filtrar os funcionários ativos (e públicos) do Facebook utilizando **axios + async/await + Promise.all()**

- Uso do **local storage** do browser para manter os itens selecionados "salvos" mesmo que o usuário mude de tela ou dê um refresh na página.
Com isso, o usuário pode navegar entre as páginas e garantir que comprou efetivamente todos os funcionários que deseja.

- Captura e tratamento de erros usando **try/catch**

- Testes unitários com **Jest + Enzyme**

## Algumas otimizações
- Separar os funcionários em "páginas" (mostrando talvez 8 ~ 10 funcionários por página). Isso garante que só carreguemos o perfil dos funcionários até aonde o usuário chegar.

- Adicionar algum tipo de ordenação (por preço, repos etc.)

## Para utilizar o projeto
- Basta clonar esse repo, instalar as dependências, seguir as instruções abaixo e mandar bala o/

`npm start`
Runs the app in the development mode.<br /> Open http://localhost:3000 to view it in the browser.

The page will reload if you make edits.<br /> You will also see any lint errors in the console.

`npm test`
Launches the test runner in the interactive watch mode.<br /> See the section about running tests for more information.