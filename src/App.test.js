import React from 'react';
import styled from 'styled-components';
import 'jest-enzyme';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

import App from './App';

describe('<App />', () => {
    it('<App /> is rendering correctly', () => {
        let wrapper = shallow(<App />);
        expect(wrapper).toMatchSnapshot();
    })
})