import React, { useState, useEffect } from 'react';
import {Route, Switch} from 'react-router-dom';
import axios from 'axios';

import CardsContainer from './Containers/CardsContainer/CardsContainer';
import Hero from './Presentational/Hero/Hero';
import Spinner from './Presentational/UI/Spinner/Spinner';
import Checkout from './Containers/Checkout/Checkout';
import Thanks from './Presentational/Thanks/Thanks';
import ErrorComponent from './Presentational/ErrorComponent/ErrorComponent';

import './App.css';

function App() {

  const [members, setMembers] = useState(null);
  const [loading, setLoading] = useState(true);
  const [purchaseList, setPurchaseList] = useState(JSON.parse(localStorage.getItem("purchase")) || []);
  const [purchasedItem, setPurchasedItem] = useState({});
  const [modalResume, setModalResume] = useState(false);
  const [error, setError] = useState(false);

  useEffect( () => {
    getCompanyMembers();
  }, [])

  const onBuyingClickHandler = (member, price) => {
    const pricedMember = {...member,price};
    const updatedList = [...purchaseList, pricedMember];

    setPurchaseList(updatedList);
    // --------------------------------------------------------------
    localStorage.setItem("purchase", JSON.stringify(updatedList));
    // --------------------------------------------------------------
    setPurchasedItem(pricedMember);
    setModalResume(true);
  };

  const onDeletePurchasedItemClick = (member) => {
    const oldPurchaseList = [...purchaseList];
    const updatedList = oldPurchaseList.filter( (item) => {
      return item.id !== member.id;
    })

    setPurchaseList(updatedList);
    // --------------------------------------------------------------
    localStorage.setItem("purchase", JSON.stringify(updatedList));
    localStorage.setItem(`${member.name} bought?`, false)
    // --------------------------------------------------------------
  }

  const onClosingModalClickHandler = () => {
    setModalResume(false);
  }

  const getCompanyMembers = async() => {
    try {
      let response = (await axios.get("https://api.github.com/orgs/facebook/members?per_page=100")).data.slice(0,6);
      let membersPromises = response.map( (member) => {
        const profile = axios.get(member.url);
        return profile;
      });
  
      const responseArray = await Promise.all(membersPromises);
      const membersArray = responseArray.map( response => {
        return response.data;
      });

      setMembers(membersArray);
      setLoading(false);

    } catch (error) {
      setError(true);
    }
  };

  return (
    <div className="App">
      <main>
      <Switch>
          <Route path="/checkout">
            <Checkout 
              purchaseList={purchaseList}
              onDeletePurchasedItemClick={onDeletePurchasedItemClick}
            />
          </Route>
          <Route path="/thanks" component={Thanks}/>
          <Route path="/">
            <Hero />
            {error ? <ErrorComponent errorMessage={<p>Não estamos conseguindo pegar a lista de funcionários disponíveis. <strong>Tente novamente mais tarde.</strong></p> } /> :
              loading ? 
                <Spinner/> : 
                <CardsContainer 
                  members={members}
                  purchasedItem={purchasedItem}
                  purchaseList={purchaseList}
                  modalResume={modalResume} 
                  onBuyingClickHandler={onBuyingClickHandler}
                  onDeletePurchasedItemClick={onDeletePurchasedItemClick}
                  onClosingModalClickHandler={onClosingModalClickHandler}  
                />
            }
          </Route>
        </Switch>
      </main>
    </div>
  )
};

export default App;
