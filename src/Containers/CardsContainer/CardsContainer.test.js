import React from 'react';
import styled from 'styled-components';
import 'jest-enzyme';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

import CardsContainer from './CardsContainer';
import Card from '../Card/Card';
import ShoppingCart from '../../Presentational/Cart/Cart';
import Modal from '../../Presentational/UI/Modal/Modal';

describe("<CardsContainer />", () => {
    let wrapper;
    beforeEach( () => {
        const members = [
                            {   id:"505",
                                src:"https://avatars1.githubusercontent.com/u/60795114?s=460&u=bb8d1826732dc6d7e1eedc53cde08c465e86dfcb&v=4", 
                                alt:"Otavio" ,
                                name:"Otavio Bonassi" ,
                                repos:"8" ,
                                followers:"1000" ,
                            },
                            {   id:"506" ,
                                src:"https://avatars1.githubusercontent.com/u/60795114?s=460&u=bb8d1826732dc6d7e1eedc53cde08c465e86dfcb&v=4", 
                                alt:"Marcos" ,
                                name:"Marcos e Belutti" ,
                                repos:"18" ,
                                followers:"1008" ,
                            },
                            {   id:"507" ,
                                src:"https://avatars1.githubusercontent.com/u/60795114?s=460&u=bb8d1826732dc6d7e1eedc53cde08c465e86dfcb&v=4" ,
                                alt:"Matheus" ,
                                name:"Matheus e Kauan" ,
                                repos:"28" ,
                                followers:"2008" ,
                            }
                        ];

        const purchaseList = [
            {   id:"505",
                src:"https://avatars1.githubusercontent.com/u/60795114?s=460&u=bb8d1826732dc6d7e1eedc53cde08c465e86dfcb&v=4", 
                alt:"Otavio" ,
                name:"Otavio Bonassi" ,
                repos:"8" ,
                followers:"1000" ,
            }];

        wrapper = shallow(<CardsContainer members={members} purchaseList={purchaseList} modalResume={true}/>);
    });


    it('Shows the correct number of items in cart', () => {
        expect(wrapper.find('shoppingCart').children()).toHaveText('1');
    })

    it('Shows the correct number of cards', () => {
        expect(wrapper.find('Card')).toHaveLength(3);
    })

    it('Renders properly', () => {
        expect(wrapper).toMatchSnapshot();
    })
})