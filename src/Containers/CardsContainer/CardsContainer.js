import React, { Component } from 'react';
import './CardsContainer.css';

import Card from '../Card/Card';
import ShoppingCart from '../../Presentational/Cart/Cart';
import Modal from '../../Presentational/UI/Modal/Modal';

class CardsContainer extends Component {

    componentWillMount() {
        let membersArr = [...this.props.members]
        this.setState({availableMembers:membersArr})
    }

    render() {
        let membersProfiles = [...this.state.availableMembers];

        let membersCards = membersProfiles.map( member => {
            return (
                <Card
                    key={member.id}
                    id={member.id}
                    src={member.avatar_url} 
                    alt={member.name} 
                    name={member.name} 
                    location={member.location}
                    repos={member.public_repos}
                    followers={member.followers}
                    add = {this.props.onBuyingClickHandler}
                    delete = {this.props.onDeletePurchasedItemClick}
                />);
        });

        return(
            <div>
                <h1 className="calltoaction">As pessoas abaixo estão disponíveis. Quais você vai comprar pro seu time?</h1>            
                {/* ------------------------------------------------------------------------------------ */}
                <ShoppingCart>{localStorage.getItem("purchase") ? JSON.parse(localStorage.getItem("purchase")).length : this.props.purchaseList.length}</ShoppingCart>
                {/* ------------------------------------------------------------------------------------ */}                
                <Modal 
                    show={this.props.modalResume} 
                    addMember={this.props.purchasedItem} 
                    onClosingModalClickHandler={this.props.onClosingModalClickHandler}
                    // finalize={this.finalize}
                    purchaseList={this.props.purchaseList}
                />
                <div className="CardsContainer">
                    {membersCards}
                </div>
            </div>
        );
    }
}

export default CardsContainer;