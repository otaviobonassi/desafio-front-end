import React, {Fragment, useState, useEffect} from 'react';
import './Checkout.css';

import Item from '../../Presentational/ItemCheckout/Item';
import NothingHere from '../../Presentational/NothingHereYet/NothingHereYet';
import FinalStep from '../../Presentational/UI/Buttons/FinalStep';
import PreviousStep from '../../Presentational/UI/Buttons/PreviousStep';


const Checkout = (props) => {

    // ------------------------------------------------------------------------------------------
    const [purchase, setPurchase] = useState(JSON.parse(localStorage.getItem("purchase")) || props.purchaseList);
    // ------------------------------------------------------------------------------------------
    
    useEffect( () => {
        setPurchase(props.purchaseList);
    })

    const totalPrice = purchase.reduce( (acc, curr) => {
        return acc + curr.price;
    },0);

    let purchaseDetails = [];
    if(purchase.length > 0) {
        purchaseDetails = purchase.map( employee => {
            return (
                <Fragment key={employee.id}>
                    <Item 
                        id={employee.id} 
                        src={employee.src} 
                        alt={employee.name} 
                        name={employee.name} 
                        repos={employee.repos} 
                        followers={employee.followers} 
                        price={employee.price}
                        click={props.onDeletePurchasedItemClick}
                    />
                </Fragment>
            )
        });
    } else {
        purchaseDetails = <NothingHere />
    }

    let totalCost = null;
    if(purchaseDetails.length > 0) {
        totalCost =
            <>
                <hr/>
                <div>
                    <h1 className="TotalPrice">TOTAL DA COMPRA: <strong>R${totalPrice.toFixed(2)}</strong></h1>
                    <div className="ButtonsControl">
                        <PreviousStep url="/">COMPRAR<br/>MAIS</PreviousStep>
                        <FinalStep url="/thanks" click={() => {localStorage.clear()}}>FINALIZAR<br/>COMPRA</FinalStep>
                    </div>
                </div>
            </>
    }

    return(
        <section className="Container">
            <h1 className="Title">Esse é um resumo da sua compra:</h1>
            {purchaseDetails}
            {totalCost}
        </section>
    );
};

export default Checkout;