import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import 'jest-enzyme';
import {configure, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

import Checkout from './Checkout';
import Item from '../../Presentational/ItemCheckout/Item';
import NothingHere from '../../Presentational/NothingHereYet/NothingHereYet';
import FinalStep from '../../Presentational/UI/Buttons/FinalStep';
import PreviousStep from '../../Presentational/UI/Buttons/PreviousStep';

describe( '<Checkout />', () => {
    let mounted;
    beforeEach( () => {
        const item1 = { 
            id:"505",
            src:"https://avatars1.githubusercontent.com/u/60795114?s=460&u=bb8d1826732dc6d7e1eedc53cde08c465e86dfcb&v=4", 
            alt:"Otavio" ,
            name:"Otavio Bonassi" ,
            repos:"8" ,
            followers:"1000" ,
            price:1 ,
        };
        const item2 = { 
            id:"506" ,
            src:"https://avatars1.githubusercontent.com/u/60795114?s=460&u=bb8d1826732dc6d7e1eedc53cde08c465e86dfcb&v=4", 
            alt:"Marcos" ,
            name:"Marcos e Belutti" ,
            repos:"18" ,
            followers:"1008" ,
            price:2  
        };
        const item3 = { 
            id:"507" ,
            src:"https://avatars1.githubusercontent.com/u/60795114?s=460&u=bb8d1826732dc6d7e1eedc53cde08c465e86dfcb&v=4" ,
            alt:"Matheus" ,
            name:"Matheus e Kauan" ,
            repos:"28" ,
            followers:"2008" ,
            price: 3  
        };

        mounted = mount( 
            <BrowserRouter>
                <Checkout 
                    purchaseList={[item1, item2, item3]}
                />
            </BrowserRouter>
        )
    })

    it('has the wright number of displayed items', () => {
        expect(mounted).toContainMatchingElements(3, 'itemDetail');
    })
    
    it('has the correct total price', () => {
        expect(mounted.find('.TotalPrice')).toIncludeText('R$6.00');
    })

    it('renders properly', () => {
        expect(mounted).toMatchSnapshot();
    })
})