import React from 'react';
import styled from 'styled-components';
import 'jest-enzyme';
import {configure, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});


import Card from './Card';
import BuyingButton from '../../Presentational/UI/Buttons/BuyingButton';
import Check from '../../Assets/images/check.png';

describe('<Card />', () => {
    let mounted;

    beforeEach( () => {
        mounted = mount(
            <Card 
                id="505" 
                src="https://avatars1.githubusercontent.com/u/60795114?s=460&u=bb8d1826732dc6d7e1eedc53cde08c465e86dfcb&v=4" 
                alt="Otavio" 
                name="Otavio Bonassi"
                location="São Paulo"
                repos="8" 
                followers="1000" 
            />)
    });

    it('Check if check icon and "cancel button" dont appear when bought state is false (meaning that the member was added to purchase list', () => {
        expect(mounted.find('[src="check.png"]').at(1)).toHaveStyle('visibility', 'hidden');
    })

    it('Renders properly', () => {
        expect(mounted).toMatchSnapshot();
    })
})