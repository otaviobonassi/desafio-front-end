import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

import BuyingButton from '../../Presentational/UI/Buttons/BuyingButton';
import Check from '../../Assets/images/check.png';

const CardBody = (styled.div)`
    position: relative;
    border-radius: 8px;
    width: 23%;
    min-width: 250px;
    margin: 40px;
    -webkit-box-shadow: 11px 15px 19px -16px rgba(94,94,94,1);
    -moz-box-shadow: 11px 15px 19px -16px rgba(94,94,94,1);
    box-shadow: 11px 15px 19px -16px rgba(94,94,94,1);
`

const Avatar = (styled.img)`
    width: 100%;
    border-radius: 8px 8px 0 0;
`

const Bought = (styled.img)`
    border-radius: 8px 8px 0 0;
    z-index: 2;
    box-sizing: border-box;
    position: absolute;
    background: rgba(0,0,0,0.6);
    padding: 30%;
    left: 0;
    width: 100%;
    visibility: hidden;
`

const Divider = (styled.hr)`
    width: 85%;
    margin: 5% 0;
    padding: 0;
    border: 0.5px solid aliceblue;
`
const Content = (styled.div)`
    width: 90%;
    text-align: left;
    margin: 3% auto;
    display: flex;
    flex-direction: column;
    align-items: center;
`

const Name = (styled.h3)`
    padding: 3% 0;
    font-family: Montserrat;
    font-weight: 800;
    font-size: 1.2rem;
    letter-spacing: 0.15rem;
`

const City = (styled.h3)`
    padding: 0 0 3% 0;
    font-family: Montserrat;
    font-weight: 400;
`

const Info = (styled.h3)`
    padding: 3% 0 3% 0;
    font-family: Montserrat;
    font-weight: 400;
`

const Price = (styled.h2)`
    font-family: Montserrat;
    font-size: 1.5rem;
    font-weight: 800;
    padding: 10%;
    color: mediumseagreen;
`


const Card = (props) => {

    const [bought, setBought] = useState(localStorage.getItem(`${props.name} bought?`) || false);

    useEffect( () => {
        localStorage.setItem(`${props.name} bought?`, bought)
    })

    const boughtItem = () => {
        setBought(true);
    }

    const cancelItem = () => {
        setBought(false);
    }

    const show = (bought==="false" || bought === false) ? {visibility: 'hidden'} : {visibility:'visible'};
  
    let price = 2*parseInt(props.repos) + parseInt(props.followers); 
    
    return(
        <CardBody>
            <Avatar src={props.src} alt={props.alt} />
            <Bought src={Check} style={show}/>
            <Content>
                <Name>{props.name}</Name>
                <City>@{props.location ? props.location : "Em todo lugar"}</City>
                <Divider/>
                <Info>Repos. Públicos:<strong> {props.repos}</strong></Info>
                <Info>Seguidores:<strong> {props.followers}</strong></Info>
                <Price>R$ {price.toFixed(2, ",")}</Price>
            </Content>
            <BuyingButton member={props} price={price} add={props.add} delete={props.delete} available={bought} bought={boughtItem} canceled={cancelItem} />

        </CardBody>
    );

}

export default Card;
