import Spinner from './Spinner';
import React from 'react';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';

configure( {adapter: new Adapter()} );

describe('<Spinner />', () => {

    it("have 4 divs to animate properly", () => {
        const wrapper = shallow(<Spinner />);
        expect(wrapper.children('div')).toHaveLength(4);
    })
    it("have the appropried className", () => {
        const wrapper = shallow(<Spinner />);
        expect(wrapper.find("lds-ellipsis")).toBeTruthy();
    })
})