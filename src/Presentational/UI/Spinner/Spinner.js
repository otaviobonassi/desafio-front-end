import React from 'react';
import "./Spinner.css";

const spinner = (props) => {
    return (
        <div className="lds-ellipsis">
            <div style={{background:`${props.color}`}}>

            </div>
            <div style={{background:`${props.color}`}}>

            </div>
            <div style={{background:`${props.color}`}}>

            </div>
            <div style={{background:`${props.color}`}}>

            </div>
        </div>
    );
};

export default spinner;