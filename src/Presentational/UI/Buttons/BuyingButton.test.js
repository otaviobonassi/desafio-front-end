import React from 'react';
import 'jest-enzyme';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure( {adapter: new Adapter()} );

import BuyingButton from './BuyingButton';

describe('<BuyingButton />', () => {

    it('Change to "cancel" if props.available is true', () => {
        const wrapper = shallow(<BuyingButton available={true}/>);
        expect(wrapper.children()).toHaveText('CANCELAR');
    })
})