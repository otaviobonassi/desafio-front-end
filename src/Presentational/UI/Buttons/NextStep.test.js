import React from 'react';
import 'jest-enzyme';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure( {adapter: new Adapter()} );

import NextStep from './NextStep';

describe('<NextStep />', () => {

    it('Check if the button renders the correct text', () => {
        const wrapper = shallow(<NextStep url="/" children="CLICK"/>);
        expect(wrapper).toHaveText('CLICK');
    })
})