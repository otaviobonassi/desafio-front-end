import React from 'react';

import styled from 'styled-components';

const BuyingButton = (styled.button)`
background-color:SeaGreen;
height: 45px;
width:40%;
border-radius:4px;
display:inline-block;
cursor:pointer;
color:#ffffff;
font-family:Montserrat, sans-serif;
font-weight: 800;
letter-spacing: 0.05rem;
font-size:0.9rem;
margin-bottom: 5%;
text-decoration:none;

&:hover {
    background-color:limegreen;
}

&:disabled {
    background-color:#ccc;
    cursor: default;
}

&:active {
    position:relative;
    top:1px;
}
`

const CancelButton = styled(BuyingButton)`
    background:palevioletred;

    &:hover {
        background:salmon;
    }
`

const button = (props) => {

    let returnedButton =    <BuyingButton 
                                onClick={() => {
                                    props.bought();
                                    props.add(props.member, props.price);
                                }}
                                    >COMPRAR
                            </BuyingButton>;

    if(props.available === "true" || props.available === true) {
        returnedButton =    <CancelButton 
                                onClick={() => {
                                    props.canceled();
                                    props.delete(props.member);
                                }}
                                    >CANCELAR
                            </CancelButton>;
    }

    return(
        <>
            {returnedButton}
        </>
    );
}

export default button;