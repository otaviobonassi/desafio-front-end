import React from 'react';
import styled from 'styled-components';

const BuyMore = (styled.button)`
    background:dodgerblue;
    border-radius:8px;
    height: 100%;
    width:40%;
    display:inline-block;
    cursor:pointer;
    color:#ffffff;
    font-family:Montserrat, sans-serif;
    letter-spacing: 0.12rem;
    font-size:0.8rem;
    text-decoration:none;

    &:hover {
        background: deepskyblue;
    }
`

const buyMore = (props) => {
    return (
        <BuyMore onClick={() => props.click()}>{props.children}</BuyMore>
    )
}

export default buyMore;