import React from 'react';
import 'jest-enzyme';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure( {adapter: new Adapter()} );

import FinalStep from './FinalStep';

describe('<FinalStep />', () => {

    it('Check if the button renders the correct text', () => {
        const wrapper = shallow(<FinalStep url="/" children="CLICK"/>);
        expect(wrapper).toHaveText('CLICK');
    })
})