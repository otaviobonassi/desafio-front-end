import React from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

const FinishPurchase = (styled.button)`
background-color:SeaGreen;
border-radius:8px;
height: 100%;
width:100%;
display:inline-block;
cursor:pointer;
color:#ffffff;
font-family:Montserrat, sans-serif;
letter-spacing: 0.12rem;
font-size:0.8rem;
color: white;

&:hover {
    background-color:limegreen;
}

&:disabled {
    background-color:#ccc;
    cursor: default;
}
`

const button = (props) => {
    return(
        <Link to={props.url} style={{padding:'0',width:'40%'}}><FinishPurchase onClick={props.click}>{props.children}</FinishPurchase></Link>
    );
}

export default button;