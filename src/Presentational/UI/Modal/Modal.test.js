import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import 'jest-enzyme';
import {configure, mount, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure( {adapter: new Adapter()} );

import Modal from './Modal';

describe('<Modal />', () => {
    let wrapper;
    let mounter;
    beforeAll(() => {
        wrapper = shallow(<Modal show={true} addMember={{name:'Otavio'}} />);
        mounter = mount(<BrowserRouter><Modal show={true} addMember={{name:'Otavio'}} /></BrowserRouter>);
    })
    
    it('Appears when props.show is true', () => {
        expect(wrapper).toHaveStyle('transform', 'translateX(calc(0vw))');
    })

    it('Disappears when props.show is false', () => {
        wrapper.setProps({show:false});
        expect(wrapper).toHaveStyle('transform', 'translateX(-100vw)');
    })

    it('Shows the correct name', () => {
        expect(mounter.find('.ItemAdicionado').children('p')).toHaveText('Otavio foi adicionado(a) à sua lista.');
    })

    it('Renders ok', () => {
        expect(wrapper).toMatchSnapshot();
    })
})