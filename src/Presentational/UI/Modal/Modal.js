import React from 'react';
import "./Modal.css";

import BuyMore from '../Buttons/BuyMore';
import NextStep from '../Buttons/NextStep';
import BagPlus from '../../../Assets/images/bag_plusOne.png'


const modal = (props) => {

    return (
            <div className="Modal" style={{transform: props.show ? "translateX(calc(0vw))" : "translateX(-100vw)"}}>
                <div className="Background">
                    <div className="Content">
                        <div className="ItemAdicionado">
                            <img src={BagPlus} alt=""/>
                            <p><strong>{props.addMember.name}</strong> foi adicionado(a) à sua lista.</p>
                        </div>
                    </div>
                    <div className="Buttons">
                        <BuyMore click={props.onClosingModalClickHandler}>COMPRAR <br/> MAIS</BuyMore>
                        <NextStep url="/checkout" click={props.onClosingModalClickHandler}>FINALIZAR <br/> COMPRA</NextStep>
                    </div>
                </div>
            </div>
    );
}

export default modal;