import React from 'react';
import 'jest-enzyme';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

import Item from './Item';

describe( '<Item />', () => {
    let wrapper;
    beforeEach( () => {
        wrapper = shallow(
            <Item 
                id="505" 
                src="https://avatars1.githubusercontent.com/u/60795114?s=460&u=bb8d1826732dc6d7e1eedc53cde08c465e86dfcb&v=4" 
                alt="Otavio" 
                name="Otavio Bonassi" 
                repos="8" 
                followers="1000" 
                price={1}  
            />)
    });

    it('Renders properly', () => {
        expect(wrapper).toMatchSnapshot();
    })

})