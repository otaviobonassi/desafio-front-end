import React from 'react';
import styled from 'styled-components';
import trash from '../../Assets/images/trash.jpg';


const Container = (styled.div)`
    display:flex; 
    align-items:center;
    width: 100%;

    @media (max-width: 500px) {
        flex-direction: column;
        align-items: center;
        min-width: 0;
        width: 100%;
    }
`

const ItemContainer = (styled.div)`
    -webkit-box-shadow: 11px 15px 19px -16px rgba(94,94,94,1);
    -moz-box-shadow: 11px 15px 19px -16px rgba(94,94,94,1);
    box-shadow: 11px 15px 19px -16px rgba(94,94,94,1);
    display: flex;
    width: 100%;
    min-width: 800px;
    margin: auto;
    align-items: stretch;
    margin: 20px 0;
    border-radius: 8px;

    @media (max-width: 500px) {
        flex-direction: column;
        align-items: center;
        min-width: 0;
        width: 100%;
    }
`

const Infos = (styled.div)`
    width: 85%;
    height: 100%;
    text-align: left;
    display: flex;
    align-items: center;

    @media (max-width: 500px) {
        flex-direction: column;
        align-items: center;
        width: 100%;
    }
`

const Photo = (styled.img)`
    width: 100px;
    height: 100%;
    border-radius: 4px 0 0 4px;

    @media (max-width: 500px) {
        border-radius: 4px;
        margin: 10px;
    }
`

const Name = (styled.h1)`
    width: 30%;
    display: inline-block;
    margin: 0 2%;
    text-align: center;
    font-family: Montserrat, sans-serif;
    font-size: 1.2rem;
    letter-spacing: 0.05rem;
    text-transform: uppercase;
    background: rgba(0,0,0,0.8);
    color: white;
    padding: 1%;

    @media (max-width: 500px) {
        width: 80%;
        margin-bottom: 10px;
    }
`

const Stats = (styled.p)`
    font-family: Montserrat;
    font-size: 0.9rem;
    font-weight: 400;
    display: block;
    margin: 0 3%;

    @media (max-width: 500px) {
        width: 80%;
        margin-bottom: 10px;
        text-align: center;
    }
`

const Price = (styled.div)`
    display: flex;
    justify-content: center;
    align-items: center;
    background: mediumseagreen;
    color: white;
    width: 15%;
    font-family: Montserrat, sans-serif;
    font-size: 1.2rem;
    border-radius: 0 4px 4px 0;
    
    @media (max-width: 500px) {
        width: 100%;
        margin-top: 10px;
        text-align: center;
        border-radius: 0 0 4px 4px;
        height: 50px;
    }
`

const Icon = (styled.img)`
    width: 20px;
    margin-left: 3%;

    &:hover {
        cursor: pointer;
    }
`


const itemDetail = (props) => {
    return(
        <Container key={props.id}>
            <ItemContainer>
                <Infos>
                    <Photo src={props.src} alt={props.alt}/>
                    <Name>{props.name}</Name>
                    <Stats>Repos:{props.repos}</Stats>
                    <Stats>Followers:{props.followers}</Stats>
                </Infos>
                <Price>R$ {props.price.toFixed(2)}</Price>
            </ItemContainer>
            <Icon onClick={() => props.click(props)} src={trash} alt="deletar" />
        </Container>
    );
}

export default itemDetail; 