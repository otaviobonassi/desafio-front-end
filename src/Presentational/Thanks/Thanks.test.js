import React from 'react';
import 'jest-enzyme';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

import Thanks from './Thanks';

describe( '<Thanks />', () => {
    it('Renders properly', () => {
        const wrapper = shallow(<Thanks />);
        expect(wrapper).toMatchSnapshot();
    })
})