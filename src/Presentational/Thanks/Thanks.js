import React from 'react';
import './Thanks.css';

import Congrats from '../../Assets/images/congrats.png';

const thanks = (props) => {
    return (
        <div className="Parabens">
            <div className="Conteudo">
                <h1>Parabéns pela contratação. Agora seu time está completo!</h1>
                <img src={Congrats} alt="Parabens pela contratação!" />
            </div>
        </div>
    );
}

export default thanks;