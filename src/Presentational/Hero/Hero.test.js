import React from 'react';
import 'jest-enzyme';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

import Hero from './Hero';

describe( '<Hero />', () => {
    let wrapper;
    beforeEach( () => {
        wrapper = shallow(<Hero />);
    });

    it('Renders properly', () => {
        expect(wrapper).toMatchSnapshot();
    })
})