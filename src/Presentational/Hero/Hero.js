import React from 'react';

import './Hero.css';

const hero = (props) => {

    return(
        <div className="Hero">
            <div>
                <h1><span>ESCOLHA O SEU TIME DOS SONHOS</span></h1>
                <p><span>Veja a lista de profissionais disponíveis, avalie seus stats e os compre para a sua equipe.</span></p>
            </div>
        </div>
    )
}

export default hero;