import React from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';
import bag from '../../Assets/images/bag.png';

const CartContainer = (styled.div)`
    width: 40px;
    display: flex;
    flex-direction: column;
    align-items: center;
    position: fixed;
    right: 1%;
    bottom: 0;
`

const CartIcon = (styled.img)`
    width: 100%;
`

const CartCounter = (styled.h1)`
    position: relative;
    bottom: 30px;
    left: 2px;
    display:block;
    font-family: Oswald, sans-serif;
    color: white;
    padding: 3%;
`

const shoppingCart = (props) => {
    return ( 
        <Link to="/checkout">
            <CartContainer>
                <CartIcon src={bag} alt="Shopping cart" />
                <CartCounter>{props.children}</CartCounter>
            </CartContainer>
        </Link>
    );
}

export default shoppingCart;