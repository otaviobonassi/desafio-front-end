import React from 'react';
import 'jest-enzyme';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

import Cart from './Cart';

describe( '<Cart />', () => {
    let wrapper;
    beforeEach( () => {
        wrapper = shallow(
            <Cart children={10} />)
    });

    it('Shows the correct image', () => {
        expect((wrapper.children().children()).at(0)).toHaveProp('src','bag.png');
    })

    it('Shows the correct error message', () => {
        expect((wrapper.children().children()).at(1)).toHaveText('10')
    })

    it('Renders properly', () => {
        expect(wrapper).toMatchSnapshot();
    })

})