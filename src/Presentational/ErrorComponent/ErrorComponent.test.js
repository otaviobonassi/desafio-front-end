import React from 'react';
import 'jest-enzyme';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

import ErrorComponent from './ErrorComponent';

describe( '<ErrorComponent />', () => {
    let wrapper;
    beforeEach( () => {
        wrapper = shallow(
            <ErrorComponent errorMessage="Errouuuuuuuu" />)
    });

    it('Shows the correct image', () => {
        expect((wrapper.children().children()).at(0)).toContainReact(<img src="x.png" alt="x" />)
    })

    it('Shows the correct error message', () => {
        expect((wrapper.children().children()).at(1)).toHaveText('Errouuuuuuuu')
    })

    it('Renders properly', () => {
        expect(wrapper).toMatchSnapshot();
    })

})