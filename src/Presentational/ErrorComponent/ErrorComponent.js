import React from 'react';
import styled from 'styled-components';

import x from '../../Assets/images/x.png';

const ErrorContainer = (styled.div)`
    background: mistyrose;
    border-radius: 8px;
    display: flex;
    align-items: center;
    justify-content: center;
    width:45%;
    margin: 1% auto;
    padding: 0.5%;

    @media (max-width: 500px) {
        margin-top: 20px;
        width: 90%;
        justify-content: space-around;
    }
`

const XContainer = styled.div `
    width: 20%;

    @media (max-width: 500px) {
        justify-content: flex-start;
    }
`

const MessageContainer = styled.div `
    width: 80%;
    text-align: left;
    border: 1px solid #403836;
    padding: 2%;
    margin-right: 4%;
    border-radius: 8px;
    font-family: Montserrat, sans-serif;
    font-size: 0.8rem;
    letter-spacing: 0.04rem;
    line-height: 1.2rem;

    @media (max-width: 500px) {
        width: 65%;
        margin: 10px 0;
        font-size: 0.8rem;
        line-height: 1.1rem;
    }
`

const errorComponent = (props) => {
    return ( 
        <ErrorContainer>
            <XContainer>
                <img src={x} alt="x" />
            </XContainer>
            <MessageContainer>
                {props.errorMessage}
            </MessageContainer>
        </ErrorContainer>
    );
}

export default errorComponent;