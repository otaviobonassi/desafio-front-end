import React from 'react';
import 'jest-enzyme';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

import NothingHereYet from './NothingHereYet';

describe( '<NothingHereYet />', () => {
    it('Renders properly', () => {
        const wrapper = shallow(<NothingHereYet />);
        expect(wrapper).toMatchSnapshot();
    })
})