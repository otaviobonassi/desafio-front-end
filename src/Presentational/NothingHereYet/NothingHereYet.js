import React from 'react';
import {Link} from 'react-router-dom';
import './NothingHereYet.css';

import Art from '../../Assets/images/cart.png';

const nothingHere = (props) => {
    return (
        <div className="NothingHere">
            <h1>Você ainda não adicionou nenhum funcionário no seu carrinho!</h1>
            <img src={Art} alt="Adicione itens no carrinho" />
            <Link className="Link" to="/">ESCOLHER FUNCIONÁRIOS</Link>
        </div>
    )
}

export default nothingHere;